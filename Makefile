CC = clang
CFLAGS = -Wall -g
OBJS = ./main.o

all: sandbox

sandbox: $(OBJS)
	$(CC) $(CFLAGS) $^ -o $@

./%.o: ./%.cp
	$(CC) $(CFLAGS) -c $^ -o $@

clean:
	rm -rf ./*.o && rm -rf *.o

mrproper: clean
	rm -rf sandbox
